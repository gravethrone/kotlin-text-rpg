var inCombat = false
fun combatStart() {

    inCombat = true
    while (inCombat) {
        playerCombat()
    }

}

fun playerCombat() {


    playerCurrentHealth -= enemyDamage
    Thread.sleep(1000)
    println("player health is ${playerCurrentHealth}")


    playerDamageRand()
    Thread.sleep(1000)
    println("enemy health is $enemyHealth")

    if (playerCurrentHealth <= 0 ) {
        println("combat has ended, you died")
        inCombat = false

    }else if (enemyHealth <= 0) {
        println("------------------------------")
        println("enemy has been slain")

        levelUpFunction()
        lootGoldFunction()

        enemyHealth = enemyMaxHealth
        playerCurrentHealth = playerMaxHealth
        inCombat = false

    } else {
        inCombat = true
    }
}


