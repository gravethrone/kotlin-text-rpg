import kotlin.random.Random

class Player {
    var health = 100
    var attack = 50
    var totalGold = 0
    var totalExp = 0
    var expRequired = 100.0
    var level = 1
}
var player = Player()

var playerExp = player.totalExp
var expGained = Goblin.expDrop
var playerLevel = player.level
var expRequiredToLevel = player.expRequired

var playerMaxHealth = player.health
var playerCurrentHealth = playerMaxHealth

var playerBaseDamage = player.attack
var playerMinDamage = playerBaseDamage / 2
var playerMaxHit = playerBaseDamage

fun playerDamageRand() {
    var playerDamage = (playerMinDamage..playerMaxHit).random()
    enemyHealth -= playerDamage
}


fun levelUpFunction() {
    if (playerExp >= expRequiredToLevel) {
        playerLevel += 1
        playerMaxHealth += 100
        expRequiredToLevel += 100.0 * 1.1
        playerBaseDamage += 10
        playerExp = 0

        println("------------------------------")
        println("your current level is: $playerLevel")
        println("required exp for next level: $expRequiredToLevel")

    }else {
        playerExp += expGained
        println("------------------------------")
        println("you gave gained $expGained exp")
        println("current exp: $playerExp")

    }
}

